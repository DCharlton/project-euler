#2520
check_list = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
n = 2520
while True:
    n += 1

    if all(n % i == 0 for i in check_list):
        print (n)
        break
