primeList = []
current = 2


def isPrime(n):
    if n == 2:
        return True
    if n == 3:
        return True
    if n % 2 == 0:
        return False
    if n % 3 == 0:
        return False

    i = 5
    w = 2

    while i * i <= n:
        if n % i == 0:
            return False

        i += w
        w = 6 - w

    return True


while current < 2000000:
    if isPrime(current):
        primeList.append(current)
    current += 1

total = 0
for i in primeList:
    total += i

print(total)
