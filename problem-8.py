nums = []
max = 0

with open("problem-8-num.txt") as f:
    while True:
        c = f.read(1)
        if not c:
            print("End of File")
            break

        if (c != "\n"):
            nums.append(int(c))

#print(nums)
i = 0
while i <= len(nums) - 13:
    num = nums[i] * nums[i+1] * nums[i+2] * nums[i+3] * nums[i+4] * nums[i+5] * nums[i+6] * nums[i+7] * nums[i+8] * nums[i+9] * nums[i+10] * nums[i+11] * nums[i+12]
    if num > max:
        max = num

    i += 1

print(max)
