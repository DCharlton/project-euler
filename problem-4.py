max = 0

for left in range (100, 1000):
    for right in range (100, 1000):
        sum = left * right
        alt_sum = int(str(sum)[::-1])

        if sum == alt_sum:
            if sum > max:
                max = sum

print (max)
    
