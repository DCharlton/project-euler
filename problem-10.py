def is_prime(n):
    if n <= 1:
        return False
    if n <= 3:
        return True
    elif n%2 == 0 or n%3 == 0:
        return False

    i = 5

    while i * i <= n:
        if n%i == 0 or n%(i+2) == 0:
            return False

        i = i + 6

    return True

count = 1
prime = 0
sum = 0
while count <= 2000000:
    prime += 1
    if is_prime(prime):
        count += 1
        sum += prime

print(sum)
