import math

number = 600851475143
max = 0;

if number%2 == 0:
    max = 2
    number = number/2

for i in range(3, int(math.sqrt(number))+1, 2):
    if number%i == 0:
        max = i
        #print (max)
        number = number / i

print(max)
