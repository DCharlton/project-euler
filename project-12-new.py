def check_divisors(n):
    if n%2 == 0:
        n = n/2

    divisors = 1
    count = 0
    while n%2 == 0:
        count += 1
        n = n/2

    divisors = divisors * (count + 1)

    p = 3
    
