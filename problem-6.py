sum = 0
sum_square = 0

for base in range(1, 101):
    curr = base * base
    sum += curr

    sum_square += base


sum_square = sum_square * sum_square

print (sum_square - sum)
